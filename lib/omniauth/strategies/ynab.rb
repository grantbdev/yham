# frozen_string_literal: true

module OmniAuth
  module Strategies
    # OmniAuth strategy for the YNAB API
    class YNAB < OmniAuth::Strategies::OAuth2
      option :client_options, site: 'https://api.youneedabudget.com'

      uid { user_info['data']['user']['id'] }

      def user_info
        @user_info ||= access_token.get('/v1/user').parsed
      end
    end
  end
end
