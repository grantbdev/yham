# frozen_string_literal: true

# Wrapper around the Money/Monetize gem functionality with YNAB milliunits
class MilliMoney < DelegateClass(Money)
  AmountToMilliunits = proc { |amount| (amount * BigDecimal(1000)).to_i }
  MilliunitsToAmount = proc { |milliunits| milliunits / BigDecimal(1000) }

  # Wrap a Money instance in a MilliMoney instance
  def self.from_money(money)
    new(
      currency: money.currency,
      milliunits: AmountToMilliunits.call(money.amount)
    )
  end

  # Wrap the result of Monetize.parse in a MilliMoney instance
  def self.parse(*args)
    MilliMoney.from_money(Monetize.parse(*args))
  end

  def initialize(currency: Money.default_currency, milliunits:)
    super(Money.from_amount(MilliunitsToAmount.call(milliunits), currency))
  end

  def is_a?(klass)
    klass == Money || super
  end

  def milliunits
    AmountToMilliunits.call(amount)
  end
end
