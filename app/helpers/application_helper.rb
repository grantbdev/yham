# frozen_string_literal: true

# Responsible for providing helper methods to extract logic from the views
module ApplicationHelper
end
