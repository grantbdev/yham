# frozen_string_literal: true

# Responsible for updating YNAB API tokens for the given user
class RefreshYNABTokensWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(user_id)
    user = User.find(user_id)

    Users::UpdateYNABCredentials.call(
      credentials: refreshed_ynab_credentials(user.ynab_refresh_token),
      user: user
    )
  end

  private

  def refreshed_ynab_credentials(refresh_token)
    OAuth2::AccessToken.from_hash(
      ynab_oauth_client,
      refresh_token: refresh_token
    ).refresh!
  end

  def ynab_oauth_client
    OAuth2::Client.new(
      Rails.application.credentials.ynab[:client_id],
      Rails.application.credentials.ynab[:client_secret],
      site: 'https://api.youneedabudget.com'
    )
  end
end
