# frozen_string_literal: true

# Responsible for creating a new equity transaction in YNAB
# and advancing the last payment number and next payment timestamp
class AdvanceEquityWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  attr_reader :equity_tracker

  delegate :mortgage, to: :equity_tracker
  delegate :user, to: :mortgage

  def perform(equity_tracker_id)
    @equity_tracker = EquityTracker.find(equity_tracker_id)

    if equity_tracker.last_payment_number < mortgage.number_of_payments
      create_transaction
      update_equity_tracker!
    else
      equity_tracker.destroy!
    end
  end

  private

  def create_transaction
    YNAB::API.new(user.ynab_access_token).transactions.create_transaction(
      equity_tracker.budget_id,
      transaction: {
        account_id: equity_tracker.account_id,
        amount: mortgage.amortization_schedule[equity_tracker.last_payment_number].principal.milliunits,
        date: Time.use_zone(mortgage.time_zone) { Date.current },
        memo: "Equity from payment ##{equity_tracker.last_payment_number + 1} - YHAM",
        payee_name: equity_tracker.payee_name
      }
    )
  end

  def update_equity_tracker!
    equity_tracker.increment(:last_payment_number)
    equity_tracker.update!(
      next_payment_at: mortgage.start_date.in_time_zone(
        mortgage.time_zone
      ).advance(months: equity_tracker.last_payment_number)
    )
  end
end
