# frozen_string_literal: true

# Responsible for keeping all mortgage equities up-to-date in YNAB
class AdvanceEquityBatchWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform
    EquityTracker.where(
      'next_payment_at < ?', Time.zone.now
    ).select(:id).find_each do |equity_tracker|
      AdvanceEquityWorker.perform_async(equity_tracker.id)
    end
  end
end
