# frozen_string_literal: true

# Responsible for keeping all YNAB API tokens up-to-date
class RefreshYNABTokensBatchWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform
    User.select(:id).find_each do |user|
      RefreshYNABTokensWorker.perform_async(user.id)
    end
  end
end
