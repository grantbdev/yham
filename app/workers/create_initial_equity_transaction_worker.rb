# frozen_string_literal: true

# Responsible for creating an initial equity transaction in YNAB
class CreateInitialEquityTransactionWorker
  include Sidekiq::Worker

  attr_reader :equity_tracker

  delegate :mortgage, to: :equity_tracker
  delegate :user, to: :mortgage

  def perform(equity_tracker_id)
    @equity_tracker = EquityTracker.find(equity_tracker_id)

    create_initial_transaction if equity_tracker.last_payment_number.positive?
  end

  private

  def create_initial_transaction
    YNAB::API.new(user.ynab_access_token).transactions.create_transaction(
      equity_tracker.budget_id,
      transaction: {
        account_id: equity_tracker.account_id,
        amount: mortgage.amortization_schedule[equity_tracker.last_payment_number - 1].equity.milliunits,
        date: Time.use_zone(mortgage.time_zone) { Date.current },
        memo: "Equity after payment ##{equity_tracker.last_payment_number} - YHAM",
        payee_name: equity_tracker.payee_name
      }
    )
  end
end
