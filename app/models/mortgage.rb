# frozen_string_literal: true

# Represents a user's mortgage
class Mortgage < ApplicationRecord
  belongs_to :user

  has_one :equity_tracker, dependent: :destroy

  def amortization_schedule
    balance = amount
    equity = start_equity
    payment = monthly_payment
    periodic_rate = monthly_interest_rate

    schedule = Array.new(number_of_payments) do |index|
      interest = MilliMoney.from_money(balance * periodic_rate)
      principal = MilliMoney.from_money(payment - interest)
      balance = MilliMoney.from_money(balance - principal)
      equity = MilliMoney.from_money(equity + principal)

      OpenStruct.new(
        balance: balance,
        date: start_date.advance(months: index),
        equity: equity,
        interest: interest,
        number: index + 1,
        principal: principal
      )
    end

    penultimate_period, last_period = schedule.last(2)
    last_period.balance = MilliMoney.new(currency: currency, milliunits: 0)
    last_period.equity = MilliMoney.from_money(amount + start_equity)
    last_period.principal = penultimate_period.balance
    last_period.interest = MilliMoney.from_money(payment - last_period.principal)

    schedule
  end

  def amount
    MilliMoney.new(currency: currency, milliunits: amount_milliunits)
  end

  def monthly_payment
    MilliMoney.new(currency: currency, milliunits: monthly_payment_milliunits)
  end

  def start_equity
    MilliMoney.new(currency: currency, milliunits: start_equity_milliunits)
  end

  def total_cost
    MilliMoney.from_money(monthly_payment * number_of_payments)
  end

  private

  def monthly_interest_rate
    interest_rate / 12
  end

  def monthly_payment_milliunits
    a = amount_milliunits
    n = number_of_payments
    r = monthly_interest_rate

    ((r * a) / (1 - (1 + r)**-n)).round
  end
end
