# frozen_string_literal: true

# Represents a mortgage equity tracker in terms of attributes for a form
class EquityTrackerForm
  include ActiveModel::Model

  attr_accessor :account_id
  attr_accessor :budget_id
  attr_accessor :payee_name
  attr_accessor :step

  with_options if: :past_step_1? do
    validates :budget_id, presence: true
  end
  with_options if: :past_step_2? do
    validates :account_id, presence: true
    validates :payee_name, presence: true
  end

  def initialize(*args)
    super(*args)
    coerce_step
  end

  def completed?
    valid? && step == 3
  end

  def past_step_1?
    step.present? && step > 1
  end

  def past_step_2?
    step.present? && step > 2
  end

  private

  def coerce_step
    self.step = if %w[1 2 3].include?(step)
                  step.to_i
                else
                  1
                end
  end
end
