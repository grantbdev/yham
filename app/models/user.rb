# frozen_string_literal: true

# Represents an authenticated user of the application
class User < ApplicationRecord
  devise :omniauthable, omniauth_providers: %i[ynab]

  has_many :mortgages, dependent: :destroy
end
