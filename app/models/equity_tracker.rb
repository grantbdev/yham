# frozen_string_literal: true

# Represents information needed to track mortgage equity in YNAB
class EquityTracker < ApplicationRecord
  belongs_to :mortgage
end
