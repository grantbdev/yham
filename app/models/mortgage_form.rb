# frozen_string_literal: true

# Represents a user's mortgage in terms of attributes for a form
class MortgageForm
  include ActiveModel::Model

  attr_accessor :amount
  attr_accessor :interest_rate_percentage
  attr_accessor :period_in_years
  attr_accessor :start_date
  attr_accessor :start_equity
  attr_accessor :time_zone

  validates :amount_milliunits, numericality: { greater_than: 0 }
  validates :interest_rate_percentage, numericality: { greater_than: 0 }
  validates :period_in_years,
            numericality: { greater_than: 0, only_integer: true }
  validates :start_date, presence: true
  validates :start_equity_milliunits,
            numericality: { greater_than_or_equal_to: 0 }
  validates :time_zone, presence: true

  def amount_currency_iso_code
    amount_milli_money.currency.iso_code
  end

  def amount_milliunits
    amount_milli_money.milliunits
  end

  def interest_rate
    BigDecimal(interest_rate_percentage) / BigDecimal(100)
  end

  def start_equity_milliunits
    start_equity_milli_money.milliunits
  end

  private

  def amount_milli_money
    @amount_milli_money ||= MilliMoney.parse(
      amount,
      Money.default_currency,
      assume_from_symbol: true
    )
  end

  def start_equity_milli_money
    @start_equity_milli_money ||= MilliMoney.parse(
      start_equity,
      amount_currency_iso_code
    )
  end
end
