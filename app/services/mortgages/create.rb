# frozen_string_literal: true

module Mortgages
  # Responsible for creating a mortgage instance from form data
  class Create
    attr_reader :form
    attr_reader :user

    def self.call(form:, user:)
      new(form: form, user: user).call
    end

    def initialize(form:, user:)
      @form = form
      @user = user
    end

    def call
      form.valid? && create_mortgage!
    end

    private

    def create_mortgage!
      user.mortgages.create!(
        amount_milliunits: form.amount_milliunits,
        currency: form.amount_currency_iso_code,
        interest_rate: form.interest_rate,
        number_of_payments: number_of_payments,
        start_date: form.start_date,
        start_equity_milliunits: form.start_equity_milliunits,
        time_zone: form.time_zone
      )
    end

    def number_of_payments
      form.period_in_years.to_i * 12
    end
  end
end
