# frozen_string_literal: true

module EquityTrackers
  # Responsible for creating a mortgage equity tracker instance from form data
  class Create
    attr_reader :form
    attr_reader :mortgage

    def self.call(form:, mortgage:)
      new(form: form, mortgage: mortgage).call
    end

    def initialize(form:, mortgage:)
      @form = form
      @mortgage = mortgage
    end

    def call
      form.completed? && create_equity_tracker!.tap do |equity_tracker|
        CreateInitialEquityTransactionWorker.perform_async(equity_tracker.id)
      end
    end

    private

    def create_equity_tracker!
      mortgage.create_equity_tracker!(
        account_id: form.account_id,
        budget_id: form.budget_id,
        last_payment_number: last_payment_number,
        next_payment_at: next_payment_at,
        payee_name: form.payee_name
      )
    end

    def last_payment_number
      @last_payment_number ||= number_of_payments_since_start_date(
        mortgage.start_date,
        Time.use_zone(mortgage.time_zone) { Date.current }
      )
    end

    def next_payment_at
      mortgage.start_date.in_time_zone(mortgage.time_zone).advance(
        months: last_payment_number
      )
    end

    def number_of_payments_since_start_date(start, today)
      return 0 if start > today

      payments_from_past_years = (today.year - start.year) * 12
      payment_this_month = if today.day >= start.day
                             1
                           else
                             0
                           end
      payments_this_year = payment_this_month + today.month - start.month
      payments_from_past_years + payments_this_year
    end
  end
end
