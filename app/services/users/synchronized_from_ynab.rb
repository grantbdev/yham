# frozen_string_literal: true

module Users
  # Responsible for returning a persisted User instance
  # corresponding to the provided auth hash from YNAB OAuth.
  # Creates or updates records in the database as needed
  # to keep credentials synchronized with YNAB.
  class SynchronizedFromYNAB
    attr_reader :auth_hash, :user

    def self.call(auth_hash:)
      new(auth_hash: auth_hash).call
    end

    def initialize(auth_hash:)
      @auth_hash = auth_hash
    end

    def call
      @user = User.find_by(ynab_user_id: auth_hash.uid)

      synchronized_user
    end

    private

    def new_user
      User.create!(
        ynab_access_token: auth_hash.credentials.token,
        ynab_refresh_token: auth_hash.credentials.refresh_token,
        ynab_token_expires_at: Time.zone.at(auth_hash.credentials.expires_at),
        ynab_user_id: auth_hash.uid
      )
    end

    def synchronized_user
      if user.present?
        updated_user
      else
        new_user
      end
    end

    def updated_user
      UpdateYNABCredentials.call(credentials: auth_hash.credentials, user: user)

      user
    end
  end
end
