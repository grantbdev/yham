# frozen_string_literal: true

module Users
  # Responsible for updating a user's YNAB credentials in the database.
  # The provided credentials object must have the following methods:
  # - `#token` returning the user's YNAB access token
  # - `#refresh_token` for the user's YNAB refresh token
  # - `#expires_at` for the UNIX timestamp of when the YNAB access token expires
  class UpdateYNABCredentials
    def self.call(credentials:, user:)
      user.update!(
        ynab_access_token: credentials.token,
        ynab_refresh_token: credentials.refresh_token,
        ynab_token_expires_at: Time.zone.at(credentials.expires_at)
      )
    end
  end
end
