# frozen_string_literal: true

# Base controller class for the web application
class ApplicationController < ActionController::Base
  private

  def after_sign_in_path_for(_user)
    mortgages_path
  end
end
