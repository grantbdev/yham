# frozen_string_literal: true

module Users
  # Responsible for handling the OAuth callback from YNAB
  class OmniauthCallbacksController < Devise::OmniauthCallbacksController
    def ynab
      user = Users::SynchronizedFromYNAB.call(
        auth_hash: request.env['omniauth.auth']
      )

      set_flash_message(:notice, :success, kind: 'YNAB')

      sign_in_and_redirect(user)
    end

    def failure
      redirect_to(root_path)
    end
  end
end
