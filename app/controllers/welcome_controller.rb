# frozen_string_literal: true

# Responsible for welcoming new visitors
class WelcomeController < ApplicationController
  def index; end
end
