# frozen_string_literal: true

# Responsible for managing mortgages
class MortgagesController < ApplicationController
  before_action :authenticate_user!

  def index
    @mortgages = current_user.mortgages
  end

  def show
    @mortgage = current_user.mortgages.find_by(id: params[:id])
  end

  def new
    @mortgage_form = MortgageForm.new
  end

  def create
    @mortgage_form = MortgageForm.new(permitted_params)

    if create_mortgage!
      redirect_to(mortgages_path, notice: 'Mortgage added.')
    else
      render 'new'
    end
  end

  private

  def create_mortgage!
    Mortgages::Create.call(form: @mortgage_form, user: current_user)
  end

  def permitted_params
    params.require(:mortgage_form).permit(
      :amount,
      :interest_rate_percentage,
      :period_in_years,
      :start_date,
      :start_equity,
      :time_zone
    )
  end
end
