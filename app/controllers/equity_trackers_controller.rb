# frozen_string_literal: true

# Responsible for managing mortgage equity trackers
class EquityTrackersController < ApplicationController
  before_action :authenticate_user!
  before_action :find_mortgage, only: %i[new create destroy]
  before_action :initialize_form, only: %i[new create]

  def new
    prepare_form
  end

  def create
    if create_equity_tracker!
      render 'equity_trackers/create/success'
    else
      prepare_form
      render 'new'
    end
  end

  def destroy
    @mortgage.equity_tracker.destroy!
    redirect_to(mortgage_path(@mortgage), notice: 'Equity tracking removed.')
  end

  private

  def create_equity_tracker!
    EquityTrackers::Create.call(form: @equity_tracker_form, mortgage: @mortgage)
  end

  def find_accounts
    @accounts = ynab_api.accounts.get_accounts(
      @equity_tracker_form.budget_id
    ).data.accounts
  end

  def find_budgets
    @budgets = ynab_api.budgets.get_budgets.data.budgets
  end

  def find_data_for_step
    case @equity_tracker_form.step
    when 1 then find_budgets
    when 2, 3 then find_accounts
    end
  end

  def find_mortgage
    @mortgage = current_user.mortgages.find_by(id: params[:mortgage_id])
  end

  def initialize_form
    @equity_tracker_form = EquityTrackerForm.new(permitted_params)
  end

  def permitted_params
    params.fetch(:equity_tracker_form, {}).permit(
      :account_id,
      :budget_id,
      :payee_name,
      :step
    )
  end

  def prepare_form
    @equity_tracker_form.step -= 1 unless @equity_tracker_form.valid?
    find_data_for_step
  end

  def ynab_api
    YNAB::API.new(current_user.ynab_access_token)
  end
end
