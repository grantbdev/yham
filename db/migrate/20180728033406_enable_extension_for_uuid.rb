# frozen_string_literal: true

# Enable the pgcrypto extension so it can be used to generate UUIDs.
class EnableExtensionForUuid < ActiveRecord::Migration[5.2]
  def change
    enable_extension 'pgcrypto' unless extension_enabled?('pgcrypto')
  end
end
