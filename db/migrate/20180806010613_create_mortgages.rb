# frozen_string_literal: true

# Creates a mortgages table for the Mortgage model
class CreateMortgages < ActiveRecord::Migration[5.2]
  def change
    create_table :mortgages, id: :uuid do |table|
      table.bigint :amount_milliunits, null: false
      table.bigint :start_equity_milliunits, null: false
      table.date :start_date, null: false
      table.decimal :interest_rate, null: false
      table.integer :number_of_payments, null: false
      table.references :user, type: :uuid, null: false
      table.string :currency, null: false
      table.string :time_zone, null: false
      table.timestamps
    end
  end
end
