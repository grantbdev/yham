# frozen_string_literal: true

# Create a users table for the Devise-backed User model
class DeviseCreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users, id: :uuid do |table|
      table.string :ynab_access_token, null: false
      table.string :ynab_refresh_token, null: false
      table.string :ynab_user_id, null: false
      table.timestamp :ynab_token_expires_at, null: false
      table.timestamps null: false
    end

    add_index :users, :ynab_user_id, unique: true
  end
end
