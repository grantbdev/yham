# frozen_string_literal: true

# Create equity_trackers table for EquityTracker model
class CreateEquityTrackers < ActiveRecord::Migration[5.2]
  def change
    create_table :equity_trackers, id: :uuid do |table|
      table.references :mortgage, type: :uuid, null: false
      table.string :account_id, null: false
      table.string :budget_id, null: false
      table.integer :last_payment_number, null: false
      table.string :payee_name, null: false
      table.timestamp :next_payment_at, null: false
      table.timestamps
    end
  end
end
