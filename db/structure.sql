SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: pgcrypto; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;


--
-- Name: EXTENSION pgcrypto; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: ar_internal_metadata; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.ar_internal_metadata (
    key character varying NOT NULL,
    value character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: equity_trackers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.equity_trackers (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    mortgage_id uuid NOT NULL,
    account_id character varying NOT NULL,
    budget_id character varying NOT NULL,
    last_payment_number integer NOT NULL,
    payee_name character varying NOT NULL,
    next_payment_at timestamp without time zone NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: mortgages; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.mortgages (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    amount_milliunits bigint NOT NULL,
    start_equity_milliunits bigint NOT NULL,
    start_date date NOT NULL,
    interest_rate numeric NOT NULL,
    number_of_payments integer NOT NULL,
    user_id uuid NOT NULL,
    currency character varying NOT NULL,
    time_zone character varying NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.schema_migrations (
    version character varying NOT NULL
);


--
-- Name: users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.users (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    ynab_access_token character varying NOT NULL,
    ynab_refresh_token character varying NOT NULL,
    ynab_user_id character varying NOT NULL,
    ynab_token_expires_at timestamp without time zone NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: ar_internal_metadata ar_internal_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ar_internal_metadata
    ADD CONSTRAINT ar_internal_metadata_pkey PRIMARY KEY (key);


--
-- Name: equity_trackers equity_trackers_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.equity_trackers
    ADD CONSTRAINT equity_trackers_pkey PRIMARY KEY (id);


--
-- Name: mortgages mortgages_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mortgages
    ADD CONSTRAINT mortgages_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: index_equity_trackers_on_mortgage_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_equity_trackers_on_mortgage_id ON public.equity_trackers USING btree (mortgage_id);


--
-- Name: index_mortgages_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_mortgages_on_user_id ON public.mortgages USING btree (user_id);


--
-- Name: index_users_on_ynab_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_users_on_ynab_user_id ON public.users USING btree (ynab_user_id);


--
-- PostgreSQL database dump complete
--

SET search_path TO "$user", public;

INSERT INTO "schema_migrations" (version) VALUES
('20180728033406'),
('20180728034956'),
('20180806010613'),
('20180817011245');


