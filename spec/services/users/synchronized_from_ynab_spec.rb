# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Users::SynchronizedFromYNAB do
  subject { described_class }

  describe '.call' do
    let(:auth_hash) do
      OmniAuth::AuthHash.new(
        'uid' => 'ynab_user_id',
        'credentials' => {
          'token' => 'ynab_access_token',
          'refresh_token' => 'ynab_refresh_token',
          'expires_at' => 1_532_797_637
        }
      )
    end

    context 'when a user matching the YNAB user ID does not already exist' do
      it 'creates a new user' do
        expect do
          subject.call(auth_hash: auth_hash)
        end.to change(User, :count).by(1)
      end

      it 'returns a new persisted user' do
        expect(subject.call(auth_hash: auth_hash)).to(
          have_attributes(
            model_name: 'User',
            persisted?: true,
            ynab_access_token: 'ynab_access_token',
            ynab_refresh_token: 'ynab_refresh_token',
            ynab_token_expires_at: Time.zone.at(1_532_797_637),
            ynab_user_id: 'ynab_user_id'
          )
        )
      end
    end

    context 'when a user matching the YNAB user ID already exists' do
      before do
        User.create(
          ynab_access_token: 'ynab_old_access_token',
          ynab_refresh_token: 'ynab_old_refresh_token',
          ynab_token_expires_at: Time.zone.at(1_532_790_437),
          ynab_user_id: 'ynab_user_id'
        )
      end

      it 'does not create a new user' do
        expect do
          subject.call(auth_hash: auth_hash)
        end.to_not change(User, :count)
      end

      it 'returns the matching user with updated API credentials' do
        expect(subject.call(auth_hash: auth_hash)).to(
          have_attributes(
            model_name: 'User',
            persisted?: true,
            ynab_access_token: 'ynab_access_token',
            ynab_refresh_token: 'ynab_refresh_token',
            ynab_token_expires_at: Time.zone.at(1_532_797_637),
            ynab_user_id: 'ynab_user_id'
          )
        )
      end
    end
  end
end
