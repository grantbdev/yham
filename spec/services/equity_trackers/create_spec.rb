# frozen_string_literal: true

require 'rails_helper'

RSpec.describe EquityTrackers::Create do
  subject { described_class }

  describe '.call' do
    let(:user) do
      User.create(
        ynab_access_token: 'ynab_access_token',
        ynab_refresh_token: 'ynab_refresh_token',
        ynab_token_expires_at: Time.zone.tomorrow,
        ynab_user_id: 'ynab_user_id'
      )
    end
    let(:mortgage) do
      Mortgage.create(
        amount_milliunits: 100_000_000,
        currency: 'USD',
        interest_rate: BigDecimal('0.0392'),
        number_of_payments: 360,
        start_date: mortgage_start_date,
        start_equity_milliunits: 10_000_000,
        time_zone: 'Tokyo',
        user: user
      )
    end
    let(:mortgage_start_date) { 2.years.ago.in_time_zone('Tokyo').to_date }

    context 'when form is complete' do
      let(:form) do
        EquityTrackerForm.new(
          account_id: 'ynab_account_id',
          budget_id: 'ynab_budget_id',
          payee_name: 'ynab_payee_name',
          step: '3'
        )
      end

      context 'during or after the start date day in the current month' do
        specify do
          expect(CreateInitialEquityTransactionWorker).to(
            receive(:perform_async)
          )
          expect(subject.call(form: form, mortgage: mortgage)).to(
            have_attributes(
              account_id: 'ynab_account_id',
              budget_id: 'ynab_budget_id',
              last_payment_number: 25,
              mortgage_id: mortgage.id,
              next_payment_at: mortgage_start_date.in_time_zone(
                'Tokyo'
              ).advance(months: 25).utc,
              payee_name: 'ynab_payee_name'
            )
          )
        end
      end

      context 'before the start date day in the current month' do
        let(:mortgage_start_date) { 2.weeks.ago.in_time_zone('Tokyo').to_date }

        specify do
          expect(CreateInitialEquityTransactionWorker).to(
            receive(:perform_async)
          )
          expect(subject.call(form: form, mortgage: mortgage)).to(
            have_attributes(
              account_id: 'ynab_account_id',
              budget_id: 'ynab_budget_id',
              last_payment_number: 1,
              mortgage_id: mortgage.id,
              next_payment_at: mortgage_start_date.in_time_zone(
                'Tokyo'
              ).advance(months: 1).utc,
              payee_name: 'ynab_payee_name'
            )
          )
        end
      end

      context 'when the start date is in the future' do
        let(:mortgage_start_date) do
          1.day.from_now.in_time_zone('Tokyo').to_date
        end

        specify do
          expect(CreateInitialEquityTransactionWorker).to(
            receive(:perform_async)
          )
          expect(subject.call(form: form, mortgage: mortgage)).to(
            have_attributes(
              account_id: 'ynab_account_id',
              budget_id: 'ynab_budget_id',
              last_payment_number: 0,
              next_payment_at: mortgage_start_date.in_time_zone('Tokyo').utc,
              mortgage_id: mortgage.id,
              payee_name: 'ynab_payee_name'
            )
          )
        end
      end
    end

    context 'when form is incomplete' do
      let(:form) do
        EquityTrackerForm.new(
          account_id: 'ynab_account_id',
          budget_id: 'ynab_budget_id',
          payee_name: 'ynab_payee_name',
          step: '2'
        )
      end

      specify do
        expect(CreateInitialEquityTransactionWorker).to_not(
          receive(:perform_async)
        )
        expect(subject.call(form: form, mortgage: mortgage)).to eq(false)
      end
    end
  end
end
