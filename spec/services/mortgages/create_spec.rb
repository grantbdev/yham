# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Mortgages::Create do
  subject { described_class }

  describe '.call' do
    let(:user) do
      User.create(
        ynab_access_token: 'ynab_access_token',
        ynab_refresh_token: 'ynab_refresh_token',
        ynab_token_expires_at: Time.zone.tomorrow,
        ynab_user_id: 'ynab_user_id'
      )
    end

    context 'when form is valid' do
      let(:form) do
        MortgageForm.new(
          amount: '$200,000',
          interest_rate_percentage: '4.75',
          period_in_years: '30',
          start_date: 2.years.ago.in_time_zone('Hawaii').to_date.to_s,
          start_equity: '$10,000',
          time_zone: 'Hawaii'
        )
      end

      specify do
        expect(subject.call(form: form, user: user)).to(
          have_attributes(
            amount_milliunits: 200_000_000,
            currency: 'USD',
            interest_rate: BigDecimal('0.0475'),
            number_of_payments: 360,
            start_date: 2.years.ago.in_time_zone('Hawaii').to_date,
            start_equity_milliunits: 10_000_000,
            time_zone: 'Hawaii',
            user: user
          )
        )
      end
    end

    context 'when form is invalid' do
      let(:form) { MortgageForm.new }

      specify do
        expect(subject.call(form: form, user: user)).to eq(false)
      end
    end
  end
end
