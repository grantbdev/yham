# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Mortgage do
  subject do
    Mortgage.new(
      amount_milliunits: 100_000_000,
      currency: 'USD',
      interest_rate: BigDecimal('0.0392'),
      number_of_payments: 360,
      start_date: 2.years.ago.in_time_zone('Hawaii').to_date,
      start_equity_milliunits: 10_000_000,
      time_zone: 'Hawaii'
    )
  end

  describe '#amortization_schedule' do
    specify do
      expect(subject.amortization_schedule.first).to(
        have_attributes(
          balance: MilliMoney.new(currency: 'USD', milliunits: 99_853_846),
          date: subject.start_date,
          equity: MilliMoney.new(currency: 'USD', milliunits: 10_146_154),
          interest: MilliMoney.new(currency: 'USD', milliunits: 326_666),
          number: 1,
          principal: MilliMoney.new(currency: 'USD', milliunits: 146_154)
        )
      )
      expect(subject.amortization_schedule.second).to(
        have_attributes(
          balance: MilliMoney.new(currency: 'USD', milliunits: 99_707_216),
          date: subject.start_date.advance(months: 1),
          equity: MilliMoney.new(currency: 'USD', milliunits: 10_292_784),
          interest: MilliMoney.new(currency: 'USD', milliunits: 326_190),
          number: 2,
          principal: MilliMoney.new(currency: 'USD', milliunits: 146_630)
        )
      )
      expect(subject.amortization_schedule.last).to(
        have_attributes(
          balance: MilliMoney.new(currency: 'USD', milliunits: 0),
          date: subject.start_date.advance(months: 359),
          equity: MilliMoney.new(currency: 'USD', milliunits: 110_000_000),
          interest: MilliMoney.new(currency: 'USD', milliunits: 5_360),
          number: 360,
          principal: MilliMoney.new(currency: 'USD', milliunits: 467_460)
        )
      )
    end
  end

  describe '#monthly_payment' do
    specify do
      expect(subject.monthly_payment).to(
        have_attributes(
          format: '$472.82',
          milliunits: 472_820
        )
      )
    end
  end

  describe '#total_cost' do
    specify do
      expect(subject.total_cost).to(
        have_attributes(
          format: '$170,215.20',
          milliunits: 170_215_200
        )
      )
    end
  end
end
