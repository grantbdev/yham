# frozen_string_literal: true

require 'rails_helper'

RSpec.describe AdvanceEquityBatchWorker do
  include ActiveSupport::Testing::TimeHelpers

  subject { described_class.new }

  describe '.call' do
    let(:equity_tracker1) do
      EquityTracker.create(
        account_id: 'ynab_account_id',
        budget_id: 'ynab_budget_id',
        last_payment_number: 3,
        next_payment_at: Time.zone.parse('2018-07-01 00:00:00 UTC'),
        mortgage: Mortgage.create(
          amount_milliunits: 100_000_000,
          currency: 'USD',
          interest_rate: BigDecimal('0.0392'),
          number_of_payments: 360,
          start_date: Date.parse('2018-04-01'),
          start_equity_milliunits: 10_000_000,
          time_zone: 'UTC',
          user: user
        ),
        payee_name: 'ynab_payee_name'
      )
    end
    let(:equity_tracker2) do
      EquityTracker.create(
        account_id: 'ynab_account_id',
        budget_id: 'ynab_budget_id',
        last_payment_number: 3,
        next_payment_at: Time.zone.parse('2018-07-02 00:00:00 JST'),
        mortgage: Mortgage.create(
          amount_milliunits: 100_000_000,
          currency: 'USD',
          interest_rate: BigDecimal('0.0392'),
          number_of_payments: 360,
          start_date: Date.parse('2018-04-02'),
          start_equity_milliunits: 10_000_000,
          time_zone: 'Tokyo',
          user: user
        ),
        payee_name: 'ynab_payee_name'
      )
    end
    let(:equity_tracker3) do
      EquityTracker.create(
        account_id: 'ynab_account_id',
        budget_id: 'ynab_budget_id',
        last_payment_number: 3,
        next_payment_at: Time.zone.parse('2018-07-01 00:00:00 HST'),
        mortgage: Mortgage.create(
          amount_milliunits: 100_000_000,
          currency: 'USD',
          interest_rate: BigDecimal('0.0392'),
          number_of_payments: 360,
          start_date: Date.parse('2018-04-01'),
          start_equity_milliunits: 10_000_000,
          time_zone: 'Hawaii',
          user: user
        ),
        payee_name: 'ynab_payee_name'
      )
    end
    let(:user) do
      User.create(
        ynab_access_token: 'ynab_access_token',
        ynab_refresh_token: 'ynab_refresh_token',
        ynab_token_expires_at: Time.zone.tomorrow,
        ynab_user_id: 'ynab_user_id'
      )
    end

    specify do
      travel_to(Time.zone.parse('2018-07-01 00:30:00 UTC')) do
        expect(AdvanceEquityWorker).to(
          receive(:perform_async).with(equity_tracker1.id)
        )
        expect(AdvanceEquityWorker).to_not(
          receive(:perform_async).with(equity_tracker2.id)
        )
        expect(AdvanceEquityWorker).to_not(
          receive(:perform_async).with(equity_tracker3.id)
        )

        subject.perform
      end
    end
  end
end
