# frozen_string_literal: true

require 'rails_helper'

RSpec.describe AdvanceEquityWorker do
  subject { described_class.new }

  describe '.call' do
    let(:equity_tracker) do
      EquityTracker.create(
        account_id: 'ynab_account_id',
        budget_id: 'ynab_budget_id',
        last_payment_number: 25,
        next_payment_at: Time.zone.parse('2018-02-28'),
        mortgage: mortgage,
        payee_name: 'ynab_payee_name'
      )
    end
    let(:user) do
      User.create(
        ynab_access_token: 'ynab_access_token',
        ynab_refresh_token: 'ynab_refresh_token',
        ynab_token_expires_at: Time.zone.tomorrow,
        ynab_user_id: 'ynab_user_id'
      )
    end
    let(:mortgage) do
      Mortgage.create(
        amount_milliunits: 100_000_000,
        currency: 'USD',
        interest_rate: BigDecimal('0.0392'),
        number_of_payments: 360,
        start_date: Date.parse('2016-01-31'),
        start_equity_milliunits: 10_000_000,
        time_zone: 'UTC',
        user: user
      )
    end
    let(:transactions_api) { instance_double(YNAB::TransactionsApi) }
    let(:ynab_api) { instance_double(YNAB::API) }

    context 'before the end of amortization schedule' do
      specify do
        expect(YNAB::API).to(
          receive(:new).with('ynab_access_token').and_return(ynab_api)
        )
        expect(ynab_api).to receive(:transactions).and_return(transactions_api)
        expect(transactions_api).to(
          receive(:create_transaction).with(
            'ynab_budget_id',
            transaction: {
              account_id: 'ynab_account_id',
              amount: 158_570,
              date: Time.use_zone('Hawaii') { Date.current },
              memo: 'Equity from payment #26 - YHAM',
              payee_name: 'ynab_payee_name'
            }
          )
        )

        expect do
          subject.perform(equity_tracker.id)
          equity_tracker.reload
        end.to(
          change { equity_tracker.last_payment_number }.to(26).and(
            change { equity_tracker.next_payment_at }.to(
              Time.zone.parse('2018-03-31')
            )
          )
        )
      end
    end

    context 'at the end of amortization schedule' do
      before { equity_tracker.update(last_payment_number: 360) }

      specify do
        expect(YNAB::API).to_not receive(:new)

        expect do
          subject.perform(equity_tracker.id)
        end.to change { EquityTracker.count }.by(-1)
      end
    end
  end
end
