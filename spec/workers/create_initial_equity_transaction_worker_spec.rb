# frozen_string_literal: true

require 'rails_helper'

RSpec.describe CreateInitialEquityTransactionWorker do
  subject { described_class.new }

  describe '.call' do
    let(:equity_tracker) do
      EquityTracker.create(
        account_id: 'ynab_account_id',
        budget_id: 'ynab_budget_id',
        last_payment_number: last_payment_number,
        next_payment_at: mortgage.start_date.in_time_zone(
          'Hawaii'
        ).advance(months: last_payment_number),
        mortgage: mortgage,
        payee_name: 'ynab_payee_name'
      )
    end
    let(:user) do
      User.create(
        ynab_access_token: 'ynab_access_token',
        ynab_refresh_token: 'ynab_refresh_token',
        ynab_token_expires_at: Time.zone.tomorrow,
        ynab_user_id: 'ynab_user_id'
      )
    end
    let(:last_payment_number) { 25 }
    let(:mortgage) do
      Mortgage.create(
        amount_milliunits: 100_000_000,
        currency: 'USD',
        interest_rate: BigDecimal('0.0392'),
        number_of_payments: 360,
        start_date: 2.years.ago.in_time_zone('Hawaii').to_date,
        start_equity_milliunits: 10_000_000,
        time_zone: 'Hawaii',
        user: user
      )
    end
    let(:transactions_api) { instance_double(YNAB::TransactionsApi) }
    let(:ynab_api) { instance_double(YNAB::API) }

    context 'when last payment number is greater than 0' do
      specify do
        expect(YNAB::API).to(
          receive(:new).with('ynab_access_token').and_return(ynab_api)
        )
        expect(ynab_api).to receive(:transactions).and_return(transactions_api)
        expect(transactions_api).to(
          receive(:create_transaction).with(
            'ynab_budget_id',
            transaction: {
              account_id: 'ynab_account_id',
              amount: 13_800_720,
              date: Time.use_zone('Hawaii') { Date.current },
              memo: 'Equity after payment #25 - YHAM',
              payee_name: 'ynab_payee_name'
            }
          )
        )

        subject.perform(equity_tracker.id)
      end
    end

    context 'when last payment number is 0' do
      let(:last_payment_number) { 0 }

      specify do
        expect(YNAB::API).to_not receive(:new)

        subject.perform(equity_tracker.id)
      end
    end
  end
end
