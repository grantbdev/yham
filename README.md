# YHAM - You Have A Mortgage

YHAM is a web application to help people see the true value of their mortgage.
It provides an unofficial [YNAB](https://www.youneedabudget.com/) plugin that automatically tracks your home equity in YNAB as you pay off your mortgage.

## Motivation

The motivation for building YHAM is based on my personal experiences from making the decision to buy a home, and how I interpret the four rules of the YNAB method in this context.

### Rule 1: Give Every Dollar A Job

Not all jobs for your money are equally important.
Spending on where you live each month is one of the most critical jobs for money.
It also happens to be one of the largest expenses, so thoughtful planning here can save you a lot in the long term.

### Rule 2: Embrace Your True Expenses

In the same way that we should embrace our true expenses, I believe we should embrace any financial upsides when we can.
Unlike rent payments, mortgage payments increase your net worth in the form of equity.
However, a budget by itself will make these payments look the same in the context of month to month expenses.

That is where YHAM comes in.

YHAM reminds you of the full picture around your mortgage: the interest you will pay, how much longer you have to pay, and the equity you are building.
It is time to embrace this expense as an investment.
YNAB helps you feel in control of your expenses; YHAM helps you realize the benefits of your payments.
Through the use of equity tracking transactions, users will have a direct link between their mortgage payments and their increasing net worth.

### Rule 3: Roll With The Punches

Home ownership offers less flexibility than renting, which can make it difficult to roll with the punches.
However, paying a mortgage can help to increase your credit score and qualify you for home equity loans when faced with unexpected expenses.
YHAM shows users their current equity, making it easier to reach financial decisions.

### Rule 4: Age Your Money

One of the best ways to age your money is to cut down on expenses.
The interest in your mortgage payments is an expense you can cut over time by paying off your mortgage faster.
The more money you can spend monthly on the mortgage, the less interest you will pay over time which will accelerate the aging of your money.
YHAM creates an amortization schedule table that shows the interest you pay each month and the total cost of the mortgage with the given terms.
This information can help users decide when the time is right to pay down their mortgage faster.

## Constraints

Given the YNAB contest deadline, I am temporarily making the following assumptions about YHAM users to reduce system complexity:
- The user has a fixed interest rate mortgage with fixed monthly payments.
- The user pays the mortgage according to the amortization schedule and is not behind or ahead on payments.

## Future Work

In addition to eliminating the current constraints to accommodate more users, YHAM could be expanded with the following features:
- Tracking the total amount left on your mortgage as debt inside YNAB.
- Exporting the total mortgage cost as a category goal in YNAB. However, it appears that the current YNAB API does not allow this functionality.
- Comparing different mortgage terms to see how much you can save on interest while still maintaining a comfortable buffer.
